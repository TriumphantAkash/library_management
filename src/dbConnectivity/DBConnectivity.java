
package dbConnectivity;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akash
 */

public class DBConnectivity {
	static Connection conn = null;
	private String  url;
	private String user;
	private String password;
	private Statement stmt;
	
	public DBConnectivity() {
		// direct connect to database in url
		url = "jdbc:mysql://localhost:3306/";
		user = "root";
		password = "root";
		
		try {
			/* Create a connection to the local MySQL server */
			conn = DriverManager.getConnection(url, user, password);
	
			/* Create a SQL statement object and execute the query */
			stmt = conn.createStatement();
			/* Set the current database, if not already set in the getConnection */
			/* Execute a SQL statement */
			stmt.execute("use Library;");

		} 
		catch(SQLException ex) {
			System.out.println("Error in connection: " + ex.getMessage());
		}
	}

        //returns set of books serach results back to the UI
	public ResultSet getBooks(SelectQueryModel qm)
	{
		String resultStr = new String();
                ResultSet rs = null;
            
                String columns = qm.getColumns();
                String table = qm.getTable();
                String condition = qm.getCondition();
                String query = " SELECT "+columns+" FROM "+table+" NATURAL JOIN BOOK_AUTHORS NATURAL JOIN BOOK_COPIES"+" WHERE "+condition+";";
               
		try {
                rs = stmt.executeQuery(query);
		//rs.close();
        	} catch(SQLException ex) {
		resultStr = ("Error in connection: " + ex.getMessage());
                }
		System.out.println(resultStr);
		return rs;
	}
        
        //adds a new borrower in the BORROWER table in the database and returns the response to UI
        public String addBorrower(InsertQueryModel qm){
                String response = "Registered Successfully";
                int i;
            
                String values = qm.getValues();
                String table = qm.getTable();
                String condition = qm.getCondition();
                String query = " INSERT INTO "+table+" VALUES "+values+";";
               
		try {
                i = stmt.executeUpdate(query);
		//rs.close();
        	} catch(SQLException ex) {
		response = ("Error in connection: " + ex.getMessage());
                }
		System.out.println(response);
		return response;
        }
        
        //to get maximum Card_no value so as to assign a new card number
        public int getMaxCardNoValue()
        {
            int maxCardNo = 0;
            ResultSet rs = null;
            String query = "select MAX(Card_no) from BORROWER;";
            try {
                rs = stmt.executeQuery(query);
		//rs.close();
        	}catch(SQLException ex) {
                System.out.println("Error in connection: " + ex.getMessage());
            }
            try {
            if(rs != null){
                    while(rs.next()){
                    maxCardNo = rs.getInt("MAX(Card_no)");
                    }
                }
            }catch (SQLException ex) {
                    Logger.getLogger(DBConnectivity.class.getName()).log(Level.SEVERE, null, ex);
                }
          
            //will return 0 in case of no tuple (I think)
            return maxCardNo;
        }
       
        public boolean loginToDatabase(int cardNum, String pswd) throws SQLException
        {
            ResultSet rs = null;
           String query = "select * from BORROWER WHERE Card_no = "+cardNum+" AND Password = "+"'"+pswd+ "'"+";";
           try {
                rs = stmt.executeQuery(query);
		//rs.close();
        	}catch(SQLException ex) {
                System.out.println("Error in connection while login: " + ex.getMessage());
            }
           
           if (!rs.next() ) {
                System.out.println("no data");
                return false;
            }
           return true;
        }
}