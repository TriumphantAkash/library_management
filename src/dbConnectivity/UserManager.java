/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbConnectivity;

/**
 *
 * @author akash
 */
public class UserManager {
    private static int cardNumber;
    private static boolean state;  //logged in: true, not logged in:false
    
    public UserManager()//default card # and state
    {
        cardNumber = 0;
        state = false;
    }
    
    public void setState(boolean b)
    {
        state = b;
    }
    
    public boolean getState()
    {
        return state;
    }
    
    public void setCardNumber(int cardNum)
    {
        cardNumber = cardNum;
    }
    
    public int getCardNumber()
    {
        return cardNumber;
    }
}
