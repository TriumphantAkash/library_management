/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbConnectivity;

/**
 *
 * @author akash
 */
public class SelectQueryModel {
    private String condition;
    private String table;
    private String columns;
    
    public void setQueryModel(String columns, String table, String condition){
        this.condition = condition;
        this.table = table;
        this.columns = columns;
    }
    
    public String getCondition(){
//        QueryModel qm = new QueryModel();
//        qm.condition = this.condition;
//        qm.columns = this.columns;
//        qm.table = this.table;
//        
//        return qm;
        return condition;
    }
    
    public String getTable()
    {
        return table;
    }
    
    public String getColumns()
    {
        return columns;
    }
    
}
