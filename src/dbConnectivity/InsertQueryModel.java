/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbConnectivity;

/**
 *
 * @author akash
 */
public class InsertQueryModel {
    


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    private String condition;
    private String table;
    private String values;
    
    public void setInsertQueryModel(String table, String values, String condition){
        this.condition = condition;
        this.table = table;
        this.values = values;
    }
    
    public String getCondition(){
//        QueryModel qm = new QueryModel();
//        qm.condition = this.condition;
//        qm.columns = this.columns;
//        qm.table = this.table;
//        
//        return qm;
        return condition;
    }
    
    public String getTable()
    {
        return table;
    }
    
    public String getValues()
    {
        return values;
    }
    
}
