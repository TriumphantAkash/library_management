/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbConnectivity;

/**
 *
 * @author akash
 */
public class LoanDataModel {
    private String bookID;
    private String title;
    private String authors;
    private String branchId;
    private int noOfCopies;
    private int noOfAvailableCopies;
    
    public void setLoanData(String bookID, String title, String authors,String branchId,int noOfCopies,int noOfAvailableCopies)
    {
      this.bookID = bookID;
      this.title = title;
      this.authors = authors;
      this.branchId = branchId;
      this.noOfCopies = noOfCopies;
      this.noOfAvailableCopies = noOfAvailableCopies;
    }
}
