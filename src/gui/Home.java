
package gui;

import dbConnectivity.DBConnectivity;
import dbConnectivity.InsertQueryModel;
import dbConnectivity.LoanDataModel;
import dbConnectivity.SelectQueryModel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import dbConnectivity.UserManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import gui.CheckOut;
import java.util.ArrayList;

/**
 *
 * @author akash
 */
public class Home extends javax.swing.JFrame {

    DefaultTableModel model ;
    private String bookSearchResult;
    private DBConnectivity db;
    SelectQueryModel qm;
    UserManager userManager = new UserManager();
    
    public Home() {
        initComponents();
        tabbedPanel.setSelectedIndex(3);
        signOutButton.setVisible(false);
        checkoutErrorLabel.setVisible(false);
        model = (DefaultTableModel) jTable1.getModel();
        
        //initalize the database by calling constructor of the DBConnectivity class
		 db = new DBConnectivity();
                 System.out.println(bookSearchResult+"*****************************************************************************************");
                 
                 //contains all the onClickListers of the buttons, textFields etc.
                // onClickListeners();
                   cardNumberText.addFocusListener(new FocusListener() {
                             @Override
                             public void focusGained(FocusEvent e) {
                                //displayMessage("Focus gained", e);
                                 loginFailureLabel.setText("");
                                 
                    }

                             @Override
                                public void focusLost(FocusEvent e) {
                                //displayMessage("Focus lost", e);
                                    loginFailureLabel.setText("");
                        }
                   });
                   
                    passwordText.addFocusListener(new FocusListener() {
                             @Override
                             public void focusGained(FocusEvent e) {
                                //displayMessage("Focus gained", e);
                                 loginFailureLabel.setText("");
                                 
                    }

                             @Override
                                public void focusLost(FocusEvent e) {
                                //displayMessage("Focus lost", e);
                                    loginFailureLabel.setText("");
                        }
                   });
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPanel = new javax.swing.JTabbedPane();
        checkInPanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        checkFinePanel = new javax.swing.JPanel();
        registerPanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        firstNameText = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        lastNameText = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        addressText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        phoneText = new javax.swing.JTextField();
        submitButton = new javax.swing.JButton();
        registrationResponseLabel = new javax.swing.JLabel();
        registerPasswordLabel = new javax.swing.JLabel();
        registerPasswordText = new javax.swing.JTextField();
        searchBookPanel = new javax.swing.JPanel();
        bookIdLabel = new javax.swing.JLabel();
        bookIdText = new javax.swing.JTextField();
        bookTitleLabel = new javax.swing.JLabel();
        bookAuthorLabel = new javax.swing.JLabel();
        bookTitleText = new javax.swing.JTextField();
        bookAuthorText = new javax.swing.JTextField();
        bookSearchErrorLabel = new javax.swing.JLabel();
        searchButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        checkoutButton = new javax.swing.JButton();
        checkoutErrorLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        cardNumberLabel = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        cardNumberText = new javax.swing.JTextField();
        passwordText = new javax.swing.JTextField();
        LoginButton = new javax.swing.JButton();
        loginFailureLabel = new javax.swing.JLabel();
        newUser = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        signOutButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tabbedPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tabbedPanel.setFont(new java.awt.Font("Khmer OS", 1, 18)); // NOI18N

        jLabel9.setText("Card no*");

        jLabel10.setText("Book TItle");

        jLabel11.setText("Book Id");

        jLabel12.setText("Book Author");

        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });

        jButton1.setText("Check In");

        javax.swing.GroupLayout checkInPanelLayout = new javax.swing.GroupLayout(checkInPanel);
        checkInPanel.setLayout(checkInPanelLayout);
        checkInPanelLayout.setHorizontalGroup(
            checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(checkInPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(checkInPanelLayout.createSequentialGroup()
                        .addGroup(checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel11)
                            .addComponent(jLabel10)
                            .addComponent(jLabel12))
                        .addGap(27, 27, 27)
                        .addGroup(checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField9, javax.swing.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE)
                            .addComponent(jTextField10)
                            .addComponent(jTextField8)
                            .addComponent(jTextField11))))
                .addContainerGap(1083, Short.MAX_VALUE))
        );
        checkInPanelLayout.setVerticalGroup(
            checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(checkInPanelLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(checkInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(592, Short.MAX_VALUE))
        );

        tabbedPanel.addTab("Check In", checkInPanel);

        javax.swing.GroupLayout checkFinePanelLayout = new javax.swing.GroupLayout(checkFinePanel);
        checkFinePanel.setLayout(checkFinePanelLayout);
        checkFinePanelLayout.setHorizontalGroup(
            checkFinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1504, Short.MAX_VALUE)
        );
        checkFinePanelLayout.setVerticalGroup(
            checkFinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 914, Short.MAX_VALUE)
        );

        tabbedPanel.addTab("Check Fine", checkFinePanel);

        jLabel5.setFont(new java.awt.Font("Khmer OS", 1, 16)); // NOI18N
        jLabel5.setText("First Name*");

        jLabel6.setFont(new java.awt.Font("Khmer OS", 1, 16)); // NOI18N
        jLabel6.setText("Last Name*");

        lastNameText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lastNameTextActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Khmer OS", 1, 16)); // NOI18N
        jLabel7.setText("Address*");

        jLabel8.setFont(new java.awt.Font("Khmer OS", 1, 16)); // NOI18N
        jLabel8.setText("Phone*");

        submitButton.setFont(new java.awt.Font("Khmer OS", 1, 16)); // NOI18N
        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        registerPasswordLabel.setText("Password*");

        javax.swing.GroupLayout registerPanelLayout = new javax.swing.GroupLayout(registerPanel);
        registerPanel.setLayout(registerPanelLayout);
        registerPanelLayout.setHorizontalGroup(
            registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registerPanelLayout.createSequentialGroup()
                .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(registerPanelLayout.createSequentialGroup()
                        .addGap(95, 95, 95)
                        .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(registerPanelLayout.createSequentialGroup()
                                    .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel5)
                                        .addComponent(firstNameText, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(18, 18, 18)
                                    .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel6)
                                        .addComponent(lastNameText)))
                                .addComponent(addressText, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(phoneText, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(registerPanelLayout.createSequentialGroup()
                                .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(registerPasswordLabel)
                                    .addComponent(registerPasswordText, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(registrationResponseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 485, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(registerPanelLayout.createSequentialGroup()
                        .addGap(353, 353, 353)
                        .addComponent(submitButton)))
                .addContainerGap(636, Short.MAX_VALUE))
        );
        registerPanelLayout.setVerticalGroup(
            registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registerPanelLayout.createSequentialGroup()
                .addGap(109, 109, 109)
                .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lastNameText, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(firstNameText, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(81, 81, 81)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addressText, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(phoneText, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(registerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(registerPanelLayout.createSequentialGroup()
                        .addComponent(registrationResponseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(submitButton))
                    .addGroup(registerPanelLayout.createSequentialGroup()
                        .addComponent(registerPasswordLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(registerPasswordText, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(299, Short.MAX_VALUE))
        );

        tabbedPanel.addTab("Register", registerPanel);

        bookIdLabel.setText("Book ID");

        bookTitleLabel.setText("Book Title");

        bookAuthorLabel.setText("Book Author");

        bookAuthorText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookAuthorTextActionPerformed(evt);
            }
        });

        searchButton.setText("SEARCH");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            },
            new String [] {
                "Book ID", "Book Title", "Book Author", "Branch ID", "Number of Copies", "Number of Available Copies", ""
            }
        ));
        jTable1.setCellSelectionEnabled(true);
        jTable1.setName("");
        jTable1.setRowHeight(45);
        jTable1.setRowMargin(3);
        jTable1.setRowSelectionAllowed(false);
        jTable1.setSelectionBackground(new java.awt.Color(255, 255, 255));
        jTable1.setSelectionForeground(new java.awt.Color(240, 240, 240));
        jScrollPane2.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(20);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(200);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(20);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(180);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(1);
        }

        checkoutButton.setText("Chekout selected Books");
        checkoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkoutButtonActionPerformed(evt);
            }
        });

        checkoutErrorLabel.setText("No book selected");

        javax.swing.GroupLayout searchBookPanelLayout = new javax.swing.GroupLayout(searchBookPanel);
        searchBookPanel.setLayout(searchBookPanelLayout);
        searchBookPanelLayout.setHorizontalGroup(
            searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(searchBookPanelLayout.createSequentialGroup()
                .addGap(46, 229, Short.MAX_VALUE)
                .addGroup(searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bookSearchErrorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(searchBookPanelLayout.createSequentialGroup()
                                .addGroup(searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bookIdLabel)
                                    .addComponent(bookIdText, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(63, 63, 63)
                                .addGroup(searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bookTitleLabel)
                                    .addComponent(bookTitleText, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(81, 81, 81)
                                .addGroup(searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(searchBookPanelLayout.createSequentialGroup()
                                        .addComponent(bookAuthorText, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(547, 547, 547)
                                        .addComponent(searchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(bookAuthorLabel)))))
                    .addComponent(checkoutButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkoutErrorLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        searchBookPanelLayout.setVerticalGroup(
            searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(searchBookPanelLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bookIdLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bookTitleLabel)
                    .addComponent(bookAuthorLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bookAuthorText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(searchBookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bookIdText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(searchButton)
                        .addComponent(bookTitleText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(31, 31, 31)
                .addComponent(bookSearchErrorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(111, 111, 111)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(checkoutErrorLabel)
                .addGap(11, 11, 11)
                .addComponent(checkoutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(255, Short.MAX_VALUE))
        );

        tabbedPanel.addTab("Search a book", searchBookPanel);

        cardNumberLabel.setText("Card #");

        passwordLabel.setText("Password");

        cardNumberText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cardNumberTextActionPerformed(evt);
            }
        });

        passwordText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passwordTextActionPerformed(evt);
            }
        });

        LoginButton.setText("Login");
        LoginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginButtonActionPerformed(evt);
            }
        });

        newUser.setText("New User");
        newUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newUserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(334, 334, 334)
                        .addComponent(LoginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(69, 69, 69)
                        .addComponent(newUser, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(passwordLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cardNumberText)
                            .addComponent(cardNumberLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(passwordText)
                            .addComponent(loginFailureLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(842, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(cardNumberLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cardNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(passwordLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(passwordText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(loginFailureLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(newUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LoginButton, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE))
                .addContainerGap(564, Short.MAX_VALUE))
        );

        tabbedPanel.addTab("Login", jPanel1);

        jLabel1.setFont(new java.awt.Font("Khmer OS", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Library Management System");

        signOutButton.setText("Sign Out");
        signOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signOutButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(359, 359, 359)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 787, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(signOutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(tabbedPanel)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(signOutButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabbedPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 971, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        String firstName = firstNameText.getText();
        String lastName = lastNameText.getText();
        String address = addressText.getText();
        String phone = phoneText.getText();
        String password = registerPasswordText.getText();
        //String password = 
        if(firstName.equals("") || lastName.equals("") || address.equals("") || phone.equals("") || password.equals("")){
            registrationResponseLabel.setText("one or more fields are enpty!");
        }else{
            int cardNumber;
            InsertQueryModel iqm = new InsertQueryModel();

            //get the maximum card nu value from the database so as to assign new card number accordingly
          int maxCardNo = db.getMaxCardNoValue();
            cardNumber = maxCardNo+1;
            //cardNumber = 1;


            //String values = "(" + cardNumber + ",'" + firstName + "','" + lastName + "'" + address + phone + ")";
            String values = "("+cardNumber+",'"+firstName+"','"+lastName+"','"+address + "','" +  phone + "','" + password + "');";
            String table = "BORROWER";

            iqm.setInsertQueryModel(table, values, null);
            String response = db.addBorrower(iqm);
            registrationResponseLabel.setText(response);
    }
        
    }//GEN-LAST:event_submitButtonActionPerformed

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed

            qm = new SelectQueryModel();
            String bookID = bookIdText.getText();
            String bookTitle = bookTitleText.getText();
            String bookAuthor = bookAuthorText.getText();
            String result = "";
            
            String columns = "Book_id, Title, Author_name, Branch_id, No_of_copies, No_of_available";
            String table = "BOOK";
            String condition1 = "Book_id LIKE "+"'%"+bookID+"%'";
            String condition2 = "Author_name LIKE "+"'%"+bookAuthor+"%'";
            String condition3 = "Title LIKE "+"'%"+bookTitle+"%'";
            
            
            //remove previous results from the table
            int rows = jTable1.getRowCount();
                     for(int i =rows-1; i>=0;i--)model.removeRow(i);
            
            String condition = "";
            if(!condition1.equals("")){
                condition = condition1+" AND ";}
            if(!condition2.equals("")){
                condition += condition2+" AND ";}
            if(!condition3.equals("")){
                condition += condition3+" AND ";}
            
            if(condition.endsWith(" AND ")){
            condition = condition.substring(0, condition.length()-5);
            }
            
            qm.setQueryModel(columns, table, condition);
            
            ResultSet rs = db.getBooks(qm);
            try {
                if(rs != null){
                    while(rs.next()){
                    result += rs.getString("Book_id") + "\t";
                    result += rs.getString("Title") + "\t";
                    result += rs.getString("Author_name") + "\t";
                    result += rs.getString("Branch_id") + "\t";
                    result += rs.getInt("No_of_copies") + "\t";
                    result += rs.getInt("No_of_available") + "\n";
                    //add to UI row
                    model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), Boolean.FALSE});
                    }
                }else
                {
                    bookSearchErrorLabel.setText("No such books found in database..");
                    System.out.println("No such books found in database..");
                }
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
            if(result.equals("")){
            bookSearchErrorLabel.setText("No such books found in database..");
                    System.out.println("No such books found in database..");
            }
            
            TableColumn includeColumn = jTable1.getColumnModel().getColumn(6);

                        includeColumn.setCellEditor(jTable1.getDefaultEditor(Boolean.class));
                        includeColumn.setCellRenderer(jTable1.getDefaultRenderer(Boolean.class)); 
                        
            
    }//GEN-LAST:event_searchButtonActionPerformed

    private void bookAuthorTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookAuthorTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bookAuthorTextActionPerformed

    private void lastNameTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lastNameTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lastNameTextActionPerformed

    private void cardNumberTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cardNumberTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cardNumberTextActionPerformed

    private void passwordTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passwordTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passwordTextActionPerformed

    private void LoginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginButtonActionPerformed
        try{
        int cardNumber = Integer.parseInt(cardNumberText.getText());
                String password = passwordText.getText();
        try {
            if(db.loginToDatabase(cardNumber, password)){   //login successfull
                userManager.setCardNumber(cardNumber);
                userManager.setState(true);
                System.out.println("login success!");
                loginFailureLabel.setText("login success!");
                //set userManager state to login
                userManager.setState(true);
                //launch Book search tab
                tabbedPanel.setSelectedIndex(3);
                signOutButton.setVisible(true);
                
            }else   //login failure
            {
                System.out.println("login failure");
                cardNumberText.setText("");
                passwordText.setText("");
                System.out.println("login failure!");
                loginFailureLabel.setText("Login failed, please try again");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
            loginFailureLabel.setText("Login failed, please try again");
            cardNumberText.setText("");
                passwordText.setText("");
        }
        }
        catch (NumberFormatException nex){
            cardNumberText.setText("");
                passwordText.setText("");
                loginFailureLabel.setText("Login failed, please try again");
        }

        
                
        
    }//GEN-LAST:event_LoginButtonActionPerformed

    private void newUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newUserActionPerformed
        // TODO add your handling code here:
        //goto Register Tab
        tabbedPanel.setSelectedIndex(2);
    }//GEN-LAST:event_newUserActionPerformed

    private void checkoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkoutButtonActionPerformed
        // TODO add your handling code here:
        //check if the user is logged in
        if(userManager.getState()){ //if the user is logged in then do checkout stuff
            int number_of_checked_books = 0;
                         CheckOut ch;
                         ArrayList <LoanDataModel> loanData = new ArrayList<LoanDataModel>();
                         LoanDataModel loanDataElement  = new LoanDataModel();
                         int i=0;
                         while(i<jTable1.getRowCount()){
                            Boolean b = ((Boolean) jTable1.getValueAt(i, 6));
                            if(b)
                            {
                                number_of_checked_books++;
                                loanDataElement.setLoanData(jTable1.getValueAt(i, 0).toString(), jTable1.getValueAt(i, 1).toString(), jTable1.getValueAt(i, 2).toString(), jTable1.getValueAt(i, 3).toString(), Integer.parseInt(jTable1.getValueAt(i, 4).toString()), Integer.parseInt(jTable1.getValueAt(i, 5).toString()));
                                loanData.add(loanDataElement);
                            }
                            i++;
                         }
                         ch = new CheckOut(loanData);
                         
                         
                         if(number_of_checked_books==0){//no book selected 
                             checkoutErrorLabel.setVisible(true);
                         } else {//some books selected
                             if(number_of_checked_books > 3){
                                 //clear checks and put error message
                                  checkoutErrorLabel.setVisible(true);
                                 checkoutErrorLabel.setText("Can't select more than 3 books");
                                 for(i=0;i<jTable1.getRowCount();i++){
                                    jTable1.setValueAt(false, i, 6);
                                 }
                                 
                             }else{
                             System.out.println("Came here!!!!!!!!!!!!!!!");
                             checkoutErrorLabel.setVisible(true);
                             checkoutErrorLabel.setText("books Checked out successfully");
                             for(i=0;i<jTable1.getRowCount();i++){
                                    jTable1.setValueAt(false, i, 6);
                                 }
                             //do checkOut stuff
                             
                             }
                         }
            
        }else   //go to login tab
        {
            tabbedPanel.setSelectedIndex(4);
        }
        
    }//GEN-LAST:event_checkoutButtonActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void signOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signOutButtonActionPerformed
        // TODO add your handling code here:
        userManager.setState(false);    //set state to false
        tabbedPanel.setSelectedIndex(3);
        signOutButton.setVisible(false);
        
    }//GEN-LAST:event_signOutButtonActionPerformed

   
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton LoginButton;
    private javax.swing.JTextField addressText;
    private javax.swing.JLabel bookAuthorLabel;
    private javax.swing.JTextField bookAuthorText;
    private javax.swing.JLabel bookIdLabel;
    private javax.swing.JTextField bookIdText;
    private javax.swing.JLabel bookSearchErrorLabel;
    private javax.swing.JLabel bookTitleLabel;
    private javax.swing.JTextField bookTitleText;
    private javax.swing.JLabel cardNumberLabel;
    private javax.swing.JTextField cardNumberText;
    private javax.swing.JPanel checkFinePanel;
    private javax.swing.JPanel checkInPanel;
    private javax.swing.JButton checkoutButton;
    private javax.swing.JLabel checkoutErrorLabel;
    private javax.swing.JTextField firstNameText;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JTextField lastNameText;
    private javax.swing.JLabel loginFailureLabel;
    private javax.swing.JButton newUser;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JTextField passwordText;
    private javax.swing.JTextField phoneText;
    private javax.swing.JPanel registerPanel;
    private javax.swing.JLabel registerPasswordLabel;
    private javax.swing.JTextField registerPasswordText;
    private javax.swing.JLabel registrationResponseLabel;
    private javax.swing.JPanel searchBookPanel;
    private javax.swing.JButton searchButton;
    private javax.swing.JButton signOutButton;
    private javax.swing.JButton submitButton;
    private javax.swing.JTabbedPane tabbedPanel;
    // End of variables declaration//GEN-END:variables
}